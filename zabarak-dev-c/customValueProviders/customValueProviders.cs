﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using Intergraph.Emea.Workflows;
using Intergraph.Emea.Ioc;
using Intergraph.Emea.Data;
using Intergraph.Emea.Placeholders;

namespace customValueProviders
{
    public class customValueProviders
    {

        [ExtensibleType("checkZamekUKM")]
        public class checkZamekUKM : IValueProvider
        {
            public object GetValue(IWorkflowControllerContext workflowControllerContext, WorkflowData workflowData, params object[] args)
            {

                using (IDatabaseProvider dbProvider = DBProviderFactory.CreateDatabaseProvider(workflowControllerContext.GetDatabaseContext().ProviderType))
                {
                    using (DbConnection connection = dbProvider.CreateConnection(workflowControllerContext.GetDatabaseContext().ConnectionString))
                    {
                        connection.Open();

                        //select zda na KÚ není nějaká rozpracovaný díl dokumentace
                        using (DbCommand seqCountValCmd = dbProvider.CreateCommand(@"select count(*) from V_ELTM_DOK_DIL where ZAMEK_TYP = 3 and KATUZE_KOD = @KATUZE_KOD and JOB_STATUS < 63", connection))
                        {
                            seqCountValCmd.Parameters.Add(dbProvider.CreateParameter("KATUZE_KOD", workflowData.GetValue(PlaceholderType.Form, "KATUZE_KOD")));

                            int readerCount = Convert.ToInt16(seqCountValCmd.ExecuteScalar());

                            if (connection.State == ConnectionState.Open) { connection.Close(); }
                            connection.Dispose();

                            if (readerCount != 0)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }



                    }
                }

            }

            public ListOfValue GetValues(IWorkflowControllerContext workflowControllerContext, WorkflowData workflowData, params object[] args)
            {
                NotImplementedException notImExc = new NotImplementedException();

                throw notImExc;
            }
        }

        [ExtensibleType("dokKontrolaStatus")]
        public class dokKontrolaStatus : IValueProvider
        {
            public object GetValue(IWorkflowControllerContext workflowControllerContext, WorkflowData workflowData, params object[] args)
            {

                var nDokId = args[0];
                if (workflowControllerContext.SessionState.Contains("IPR.dokKontrola.status." + nDokId))
                {
                    return workflowControllerContext.SessionState.Get("IPR.dokKontrola.status." + nDokId);
                }
                else
                {
                    return null;
                }

            }

            public ListOfValue GetValues(IWorkflowControllerContext workflowControllerContext, WorkflowData workflowData, params object[] args)
            {
                NotImplementedException notImExc = new NotImplementedException();

                throw notImExc;
            }
        }

    }
}
