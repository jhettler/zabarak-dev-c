﻿using System;
using System.Collections.Generic;
using System.Linq;
using Oracle.DataAccess.Client;
using Oracle.DataAccess;
using Oracle.DataAccess.Types;
using Intergraph.Emea.Data;
using Intergraph.Emea.Core;
using Intergraph.Emea.Ioc;
using Intergraph.Emea.Workflows;
using Intergraph.Emea.Placeholders;
using System.Text;
using System.Data;
using System.Runtime.Serialization;
using Intergraph.Emea.Configuration;
using Intergraph.Emea.Security;

namespace IPR.customTriggers
{

    public class customTriggers
    {
        public static string GetConnectionString(IWorkflowControllerContext workflowControllerContext)
        {
            Object o;

            try
            {
                if (workflowControllerContext.GetDatabase() == null)
                    return "workflowControllerContext.GetDatabase() is null";

                o = workflowControllerContext.GetDatabase().ConnectionString;
                if (o == null)
                    return "GetDatabase()._connectionString is null)";
                else
                {
                    string userID = o.ToString().Substring(0, o.ToString().LastIndexOf('/'));
                    string passwd = o.ToString().Substring(o.ToString().LastIndexOf('/') + 1, o.ToString().LastIndexOf('@') - o.ToString().LastIndexOf('/') - 1);
                    string dataSrc = o.ToString().Split('@')[1];
                    return "User Id=" + userID + ";Password=" + passwd + ";Data Source=" + dataSrc;
                }
                //FIX IT
                //return (string)o;

            }
            catch (Exception ex)
            {
                return "GetConnString Err=" + ex.Message;
            }


        }


        // Ukazka volani procedury s navratovymi hodnotami

        // nastaveni ve WorkflowSettings xml
        // <WorkflowTrigger name="LibovolnyNazev" type="DilDoWork" method="action">
        //    <Param name="nZamekId" value="{REQUESTCONTEXT.zamekId}"/>
        //    <Param name="nTypOperace" value="{REQUESTCONTEXT.typOperace}"/>
        // </WorkflowTrigger>		

        // priklad volani ve FormSettings xml
        // <FormAction name="vymazSouborNaServeru" label="Vymaz soubor" action="SCRIPT[IG.trigger({triggerNames:['LibovolnyNazev'], urlParams:{typOperace:{FORM.TYPOPERACE}, zamekId:{FORM.ZAMEKID}}).done(function(){IG.reloadPage()})]" help="Nějaká nápověda" type="form" visible="true" authorization="ROLE[ELTM_ADMIN,ELTM_SPRAVCEAKTUALIZACI]"/>

        [ExtensibleType("DilDoWork")]
        public class DilDoWork : WorkflowTriggerBase
        {

            public string nZamekId { get; set; }
            public string nTypOperace { get; set; }

            public override void Execute()
            {
                using (OracleConnection conn = new OracleConnection(customTriggers.GetConnectionString(WorkflowControllerContext)))

                //try
                {

                    OracleCommand cmd = new OracleCommand("INGR_IPR_PKG.DilDoWork", conn);

                    conn.Open();

                    cmd.CommandType = CommandType.StoredProcedure;
                    OracleTransaction trans = conn.BeginTransaction();
                    cmd.Transaction = trans;

                    OracleParameter InParam1 = new OracleParameter("nZamekId", OracleDbType.Decimal, ParameterDirection.Input);
                    cmd.Parameters.Add(InParam1).Value = nZamekId;

                    OracleParameter InParam2 = new OracleParameter("nTypOperace", OracleDbType.Char, 1, ParameterDirection.Input);
                    cmd.Parameters.Add(InParam2).Value = Convert.ToChar(nTypOperace);

                    OracleParameter OutParam1 = new OracleParameter("err_code", OracleDbType.Decimal, 1, ParameterDirection.Output);
                    cmd.Parameters.Add(OutParam1);

                    OracleParameter OutParam2 = new OracleParameter("err_desc", OracleDbType.Varchar2, 4000, null, ParameterDirection.Output);
                    cmd.Parameters.Add(OutParam2);

                    cmd.ExecuteNonQuery();

                    if (OutParam1.Value.ToString().Equals("null") || OutParam1.Value.Equals(null) || OutParam1.Value.ToString().Equals("0"))
                    {
                        trans.Commit();
                    }
                    else
                    {

                        trans.Rollback();
                        if (conn.State == ConnectionState.Open) { conn.Close(); }
                        conn.Dispose();

                        WorkflowException wfException = new WorkflowException("Chyba v proceduře INGR_IPR_PKG.DilDoWork  - " + OutParam1.Value + " " + OutParam2.Value);

                        throw wfException;

                    }

                }
            }



        }

        // nastaveni ve WorkflowSettings xml
        // <WorkflowTrigger name="LibovolnyNazev" type="DeleteFileInPath" method="action">
        //    <Param name="PATH" value="{REQUESTCONTEXT.path}"/>
        // </WorkflowTrigger>		
        
        // volani ve FormSettings xml
        // <FormAction name="vymazSouborNaServeru" label="Vymaz soubor" action="SCRIPT[IG.trigger({triggerNames:['LibovolnyNazev'], urlParams:{PATH:'\\'}}).done(function(){IG.reloadPage()})]" help="Nějaká nápověda" type="form" visible="true" authorization="ROLE[ELTM_ADMIN,ELTM_SPRAVCEAKTUALIZACI]"/>

        [ExtensibleType("DeleteFileInPath")]
        public class DeleteFileInPath : WorkflowTriggerBase
        {

            public string PATH { get; set; }


            public override void Execute()
            {

                if (System.IO.File.Exists(PATH))
                {
                    try
                    {
                        System.IO.File.Delete(PATH);
                    }
                    catch (System.IO.IOException e)
                    {
                        throw new WorkflowException("Nepodařilo se vymazat soubor" + PATH + ". Chyba " + e);
                    }
                }
                else
                {
                    throw new WorkflowException("Soubor" + PATH + " neexistuje.");
                }

            }
        }

    }


}


