﻿using Intergraph.Emea.Data;
using Intergraph.Emea.GeoSpatial;
using Intergraph.Emea.SmartClient.Data;
using Intergraph.Emea.Workflows;
using Intergraph.Emea.Configuration;
using Intergraph.Emea.Security;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Runtime.Serialization;
using System.Net;
using Oracle.DataAccess;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Newtonsoft.Json;
using log4net;
using log4net.Config;

namespace IPR.geometryValidations
{
    public class geometryValidations : IGeometryValidationHandler
    {

        private static readonly ILog logger = LogManager.GetLogger(typeof(geometryValidations));

        static geometryValidations()
        {
            XmlConfigurator.Configure();
        }

        public bool CanValidate(GeometryValidationContext context)
        {


            string[] entityParts = context.FeatureMetadata.Entity.Split('.');

            return entityParts.Last().Equals("TMISPRUBEH_L_WORK", StringComparison.OrdinalIgnoreCase) || entityParts.Last().Equals("TMISZNAK_B_WORK", StringComparison.OrdinalIgnoreCase) || entityParts.Last().Equals("TMISZNAK_L_WORK", StringComparison.OrdinalIgnoreCase) || entityParts.Last().Equals("TMUMPS_B_WORK", StringComparison.OrdinalIgnoreCase) || entityParts.Last().Equals("TMUMPS_L_WORK", StringComparison.OrdinalIgnoreCase) || entityParts.Last().Equals("UKMPCISLO_B_WORK", StringComparison.OrdinalIgnoreCase) || entityParts.Last().Equals("UKMPHRANICE_L_WORK", StringComparison.OrdinalIgnoreCase) || entityParts.Last().Equals("ELTM_DOK_DIL", StringComparison.OrdinalIgnoreCase) || entityParts.Last().Equals("TMMESTSKECASTI_L_WORK", StringComparison.OrdinalIgnoreCase) || entityParts.Last().Equals("TMMESTSKECASTI_B_WORK", StringComparison.OrdinalIgnoreCase) || entityParts.Last().Equals("ELTM_DOK_GRAFIKA_BOD", StringComparison.OrdinalIgnoreCase) || entityParts.Last().Equals("TMTVU_B_WORK", StringComparison.OrdinalIgnoreCase);

        }

        public void Validate(GeometryValidationContext context)
        {
            string[] entityParts = context.FeatureMetadata.Entity.Split('.');


            // ##########################
            // Validace TMUMPS_B_WORK
            #region TMUMPS_B_WORK validace
            if (entityParts.Last().Equals("TMUMPS_B_WORK", StringComparison.OrdinalIgnoreCase))
            {
                if (context.Geometries.Any(g => g.ChangeType == GeometryChange.Deleted))
                    return;

                ArrayList idArrayList = new ArrayList();
                int geometriesCounter = 0;

                foreach (Intergraph.Emea.SmartClient.Data.GeometryMetadata geometryToValidate in context.Geometries)
                {

                    Geometry originalGeometry = geometryToValidate.Geometry;

                    geometryUtils geomUtils = new geometryUtils();
                    geometriesCounter = geometriesCounter + 1;

                    // ++++ Validace, jestli existuje geometrie a povinne atributy ++++
                    geomUtils.geomAttributesOk(geometryToValidate, context);

                    // Zaokrouhli linii na 3 desetinna mista a pak s ni pracuje ve validacich                       
                    geometryToValidate.Geometry = geomUtils.roundGeometryCoords(geometryToValidate.Geometry, 3);

                    // Fixnuti chyby pri plneni TID_ sloupcu
                    geometryToValidate.Attributes["TID_TMUMPS_B"] = geometryToValidate.Attributes["ID"];

                    using (IDatabaseProvider dbProvider = DBProviderFactory.CreateDatabaseProvider(DBProviderTypeResolver.FromDataSource(context.FeatureMetadata.DataSource.AsValue())))
                    {
                        using (DbConnection connection = dbProvider.CreateConnection(context.FeatureMetadata.DataConnect))
                        {
                            connection.Open();

                            // +++++++++ Validace jestli spadne geometrie do zámku +++++++++
                            geomUtils.insideArea(geometryToValidate, geometriesCounter, context, dbProvider, connection);

                            // +++++++++ Validace na procházení přes Zka +++++++++
                            if (geometryToValidate.Attributes["VYSKOPIS"].ToString() != "0")
                            {
                                geomUtils.zLevelPointEquality(geometryToValidate, geometriesCounter, context, dbProvider, connection);
                            }

                        }
                    }

                    geometryToValidate.Geometry = originalGeometry;

                    // check if checkbox is toggled
                    /*Site gmscSite = SmartClientAppSettings.GetSite("GMSCDEV");
                    using (ISessionProvider sessionProvider = new SessionProvider(gmscSite.ConnectionString, DBProviderTypeResolver.FromType(gmscSite.ProviderName)))
                    {
                        string sessionid = context.SessionId;
                        SessionState session = sessionProvider.Fetch(sessionid);
                        var zdmt = session.Get("ZzDMT");
                        if (Convert.ToDouble(zdmt.ToString()) == 1)
                        {
                            // nastavi bodu souradnici Z z DMT
                            geometryToValidate.Geometry = geomUtils.setZCoordinateFromDMT(geometryToValidate.Geometry);
                        }

                    }*/

                }

                return;
            }
            #endregion


            // ##########################
            // Validace TMTVU_B_WORK
            #region TMDILTVU_B_WORK validace
            if (entityParts.Last().Equals("TMTVU_B_WORK", StringComparison.OrdinalIgnoreCase))
            {
                if (context.Geometries.Any(g => g.ChangeType == GeometryChange.Deleted))
                    return;

                int geometriesCounter = 0;
                ArrayList idArrayList = new ArrayList();

                foreach (Intergraph.Emea.SmartClient.Data.GeometryMetadata geometryToValidate in context.Geometries)
                {

                    Geometry originalGeometry = geometryToValidate.Geometry;

                    geometryUtils geomUtils = new geometryUtils();
                    geometriesCounter = geometriesCounter + 1;

                    // ++++ Validace, jestli existuje geometrie a povinne atributy ++++
                    geomUtils.geomAttributesOk(geometryToValidate, context);

                    // Zaokrouhli linii na 3 desetinna mista a pak s ni pracuje ve validacich                       
                    geometryToValidate.Geometry = geomUtils.roundGeometryCoords(geometryToValidate.Geometry, 3);

                    // Fixnuti chyby pri plneni TID_ sloupcu
                    geometryToValidate.Attributes["TID_TMTVU_B"] = geometryToValidate.Attributes["ID"];

                    using (IDatabaseProvider dbProvider = DBProviderFactory.CreateDatabaseProvider(DBProviderTypeResolver.FromDataSource(context.FeatureMetadata.DataSource.AsValue())))
                    {
                        using (DbConnection connection = dbProvider.CreateConnection(context.FeatureMetadata.DataConnect))
                        {
                            connection.Open();

                            // +++++++++ Validace jestli spadne geometrie do zámku +++++++++
                            geomUtils.insideArea(geometryToValidate, geometriesCounter, context, dbProvider, connection);

                            String sqlWhere;
                            if (Convert.ToInt16(geometryToValidate.Attributes["CTVUK_KOD"]) < 0)
                            {
                                sqlWhere = "CTVUK_KOD < 0";
                            }
                            else if (Convert.ToInt16(geometryToValidate.Attributes["CTVUK_KOD"]) > 0 && Convert.ToInt16(geometryToValidate.Attributes["CTVUK_KOD"]) < 10000)
                            {
                                sqlWhere = "CTVUK_KOD > 0 and CTVUK_KOD < 10000";
                            }
                            else if (Convert.ToInt16(geometryToValidate.Attributes["CTVUK_KOD"]) > 10000)
                            {
                                sqlWhere = "CTVUK_KOD > 10000";
                            }
                            else
                            {
                                sqlWhere = "1=1";
                            }

                            using (DbCommand cmd = dbProvider.CreateCommand("select count(*) from TMTVU_B_WORK g where SDO_ANYINTERACT(g.GEOMETRY,MDSYS.SDO_GEOMETRY(3001,NULL,MDSYS.SDO_POINT_TYPE(@X,@Y,0),NULL,NULL)) = 'TRUE' and " + sqlWhere, connection))
                            {
                                cmd.Parameters.Add(dbProvider.CreateParameter("X", geometryToValidate.Geometry.STX));
                                cmd.Parameters.Add(dbProvider.CreateParameter("Y", geometryToValidate.Geometry.STY));

                                int errorCount = Convert.ToInt16(cmd.ExecuteScalar());

                                if (errorCount > 0)
                                {
                                    context.Faults.Add(new Intergraph.Emea.SmartClient.Data.GeometryValidationFault(Guid.NewGuid().ToString(), "Pro B" + geometriesCounter.ToString() + " již existuje bod s totožným X,Y a hladinou"));
                                }
                            }

                            // +++++++++ Validace jestli není přítomný nějaký překryv linie se stejným CTVUK_KOD +++++++++
                            //geomUtils.coversWithSameAttributes(geometryToValidate, entityParts.Last().ToString(), context, geometriesCounter, "CTVUK_KOD", dbProvider, connection, idArrayList);

                            // +++++++++ Kontrola, jestli nepřekrývá bod TMDILTVU3D_B_WORK +++++++++
                            //geomUtils.checkCoexistenceForTMDILTVU3DB(geometryToValidate, geometriesCounter, context, dbProvider, connection);

                        }
                    }

                    geometryToValidate.Geometry = originalGeometry;

                }
                return;
            }
            #endregion




            // ##########################
            // Validace ELTM_DOK_GRAFIKA_BOD
            #region ELTM_DOK_GRAFIKA_BOD validace
            if (entityParts.Last().Equals("ELTM_DOK_GRAFIKA_BOD", StringComparison.OrdinalIgnoreCase))
            {
                if (context.Geometries.Any(g => g.ChangeType == GeometryChange.Deleted))
                    return;

                geometryUtils geomUtils = new geometryUtils();

                int geometriesCounter = 0;
                using (IDatabaseProvider dbProvider = DBProviderFactory.CreateDatabaseProvider(DBProviderTypeResolver.FromDataSource(context.FeatureMetadata.DataSource.AsValue())))
                {
                    using (DbConnection connection = dbProvider.CreateConnection(context.FeatureMetadata.DataConnect))
                    {
                        connection.Open();

                        foreach (Intergraph.Emea.SmartClient.Data.GeometryMetadata geometryToValidate in context.Geometries)
                        {


                            /// ++++ Prepocet hodnot vysek Z a nastaveni atributu PUVOD_Z ++++
                            Site gmscSite;
                            if (Environment.MachineName == "ELTMV")
                            {
                                gmscSite = SmartClientAppSettings.GetSite("GMSCDEV");

                            }
                            else
                            {
                                gmscSite = SmartClientAppSettings.GetSite("GMSC");
                            }

                            using (ISessionProvider sessionProvider = new SessionProvider(gmscSite.ConnectionString, DBProviderTypeResolver.FromType(gmscSite.ProviderName)))
                            {
                                string sessionid = context.SessionId;
                                SessionState session = sessionProvider.Fetch(sessionid);
                                var zdmt = session.Get("ZzDMT");
                                if (zdmt == null) zdmt = 0;

                                logger.DebugFormat("Site settings: {0} || Feature: {1} || IsDMTenabled: {2}", gmscSite.Name.ToString(), entityParts.Last().ToString(), zdmt);
                                logger.DebugFormat("Original PUVOD_Z: {0} || IsDMTenabled: {1} || Original height (Z) value: {2}", geometryToValidate.Attributes["PUVOD_Z"], zdmt, geometryToValidate.Geometry.Z);

                                if (zdmt.ToString() == "1" || geometryToValidate.Geometry.Z == 0)
                                {
                                    // nastavi bodu souradnici Z z DMT
                                    geometryToValidate.Geometry = geomUtils.setZCoordinateFromDMT(geometryToValidate.Geometry);
                                    geometryToValidate.Attributes["PUVOD_Z"] = 3;

                                    logger.DebugFormat("Changing PUVOD_Z to value 3 || IsDMTenabled: {0} || Height (Z) value: {1}", zdmt, geometryToValidate.Geometry.Z);

                                }
                                else if (geometryToValidate.Attributes["PUVOD_Z"] == null)
                                {
                                    geometryToValidate.Attributes["PUVOD_Z"] = 1;
                                    logger.DebugFormat("Changing PUVOD_Z to value 1 || IsDMTenabled: {0} || Height (Z) value: {1}", zdmt, geometryToValidate.Geometry.Z);

                                }
                                else
                                {
                                    logger.DebugFormat("NO changes of PUVOD_Z or height were made");
                                }


                                geometriesCounter = geometriesCounter + 1;

                                // Body nejsou poplatne zamku, ale cele dokumentaci
                                // ++++ Validace jestli spadne geometrie do zamku ++++
                                // geomUtils.insideArea(geometryToValidate, geometriesCounter, context, dbProvider, connection);

                                using (DbCommand cmd = dbProvider.CreateCommand(@"(select ID, GEOMETRY,VYSKOPIS, ID||'TMPODROBNYBOD_B' as UNIQUEID  from INGR_ZSGD.TMPODROBNYBOD_B podrb    
                                                                            where SDO_ANYINTERACT(podrb.GEOMETRY,SDO_UTIL.FROM_WKTGEOMETRY(@GEOMETRYTOVALIDATE)) = 'TRUE') 
                                                                                UNION ALL                               
                                                                            (select ID, GEOMETRY, to_number(nvl(replace(replace(dokb.VYSKOPIS,'Z_IS_NULL',0),'Z',''),0)) as VYSKOPIS ,ID||'ELTM_DOK_GRAFIKA_BOD' as UNIQUEID from ELTM_DOK_GRAFIKA_BOD dokb 
                                                                            where SDO_ANYINTERACT(dokb.GEOMETRY,SDO_UTIL.FROM_WKTGEOMETRY(@GEOMETRYTOVALIDATE)) = 'TRUE') ", connection))
                                {
                                    //je potreba vyhodit Zkove souradnice, pokud jsou, protoze oracle nema ve WKT podporu 3D
                                    RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace;
                                    cmd.Parameters.Add(dbProvider.CreateParameter("GEOMETRYTOVALIDATE", Regex.Replace(geometryToValidate.Geometry.AsTextZM().Replace(" 0", ""), "[\\s]{1}[0-9]{1,4}[\\.]{0,1}[0-9]*", "", options)));
                                    //cmd.Parameters.Add(dbProvider.CreateParameter("ID", geometryToValidate.Attributes["ID"]));
                                    using (DbDataReader reader = cmd.ExecuteReader())
                                    {
                                        if (reader.HasRows)
                                        {
                                            int failCount = 0;

                                            while (reader.Read())
                                            {

                                                var pointGeomPodrbAndDokb = dbProvider.GetValue<Geometry>(reader, "GEOMETRY");

                                                pointGeomPodrbAndDokb = geomUtils.roundGeometryCoords(pointGeomPodrbAndDokb, 3);
                                                geometryToValidate.Geometry = geomUtils.roundGeometryCoords(geometryToValidate.Geometry, 3);

                                                if (geometryToValidate.Geometry.STX == pointGeomPodrbAndDokb.STX && geometryToValidate.Geometry.STY == pointGeomPodrbAndDokb.STY && geometryToValidate.Geometry.Z == pointGeomPodrbAndDokb.Z)
                                                {
                                                    logger.DebugFormat("Can't create point coordinates X:{0},Y:{1},Z:{2} same point has existed in the DB", geometryToValidate.Geometry.STX, geometryToValidate.Geometry.STY, geometryToValidate.Geometry.Z);
                                                    logger.DebugFormat("New point coordinates X:{0},Y:{1},Z:{2}", geometryToValidate.Geometry.STX, geometryToValidate.Geometry.STY, geometryToValidate.Geometry.Z);
                                                    logger.DebugFormat("Existed point coordinates X:{0},Y:{1},Z:{2}", pointGeomPodrbAndDokb.STX, pointGeomPodrbAndDokb.STY, pointGeomPodrbAndDokb.Z);

                                                    failCount = failCount + 1;
                                                }

                                                if (failCount > 0)
                                                {
                                                    context.Faults.Add(new Intergraph.Emea.SmartClient.Data.GeometryValidationFault(Guid.NewGuid().ToString(), "Pro B" + geometriesCounter.ToString() + " již existuje bod s totožným X,Y,Z", geometryToValidate.Geometry));
                                                }
                                            }
                                        }
                                    }
                                }


                            }
                        }
                    }

                }
                return;
            }
            #endregion


        }

        public void OnGeometryChanged(GeometryChangedContext context)
        {

        }

    }

    public class geometryUtils
    {

        private static readonly ILog logger = LogManager.GetLogger(typeof(geometryUtils));

        static geometryUtils()
        {
            XmlConfigurator.Configure();
        }

        /// <summary>
        /// Rounds the GeometryMetadata object to 2 decimal places and creates rounded geometry object
        /// </summary>
        /// <param name="geometryToRound">Intergraph.Emea.SmartClient.Data.Geometry object</param>
        /// <param name="precision">number of decimal places</param>  
        public Geometry roundGeometryCoords(Geometry geometryToRound, int precision)
        {

            double coordX;
            double coordY;
            double coordZ;
            double roundedX;
            double roundedY;
            double roundedZ = 0;
            //hledám ve všech lomových bodech, jestli se někde rovná

            coordX = Convert.ToDouble(geometryToRound.STStartPoint().STX.Value.ToString());
            coordY = Convert.ToDouble(geometryToRound.STStartPoint().STY.Value.ToString());

            roundedX = Math.Round(coordX, precision);
            roundedY = Math.Round(coordY, precision);

            if (geometryToRound.Dimension > 2)
            {
                coordZ = Convert.ToDouble(geometryToRound.STStartPoint().Z.Value.ToString());
                roundedZ = Math.Round(coordZ, precision);
            }

            GeometryBuilder testGeomBuilder = new GeometryBuilder(0, geometryToRound.Dimension);
            testGeomBuilder.BeginGeometry(geometryToRound.GeometryType);

            if (geometryToRound.Dimension > 2)
            {
                testGeomBuilder.BeginFigure(roundedX, roundedY, roundedZ);

                for (int k = 2; k <= geometryToRound.STNumPoints(); k++)
                {

                    coordX = Convert.ToDouble(geometryToRound.STPointN(k).STX.Value.ToString());
                    coordY = Convert.ToDouble(geometryToRound.STPointN(k).STY.Value.ToString());
                    coordZ = Convert.ToDouble(geometryToRound.STPointN(k).Z.Value.ToString());

                    roundedX = Math.Round(coordX, precision);
                    roundedY = Math.Round(coordY, precision);
                    roundedZ = Math.Round(coordZ, precision);

                    testGeomBuilder.AddLine(roundedX, roundedY, roundedZ);
                }

            }
            else
            {
                testGeomBuilder.BeginFigure(roundedX, roundedY);

                for (int k = 2; k <= geometryToRound.STNumPoints(); k++)
                {

                    coordX = Convert.ToDouble(geometryToRound.STPointN(k).STX.Value.ToString());
                    coordY = Convert.ToDouble(geometryToRound.STPointN(k).STY.Value.ToString());

                    roundedX = Math.Round(coordX, precision);
                    roundedY = Math.Round(coordY, precision);

                    testGeomBuilder.AddLine(roundedX, roundedY);
                }
            }

            testGeomBuilder.EndFigure();
            testGeomBuilder.EndGeometry();

            return testGeomBuilder.ConstructedGeometry;
        }

        /// <summary>
        /// Validate geometry on selfIntersect (just for lines, polylines, ...)
        /// </summary>
        /// <param name="geometry">Intergraph.Emea.SmartClient.Data.Geometry object</param>
        /// <param name="geometriesCounter">Count of geometries in context.Geometries</param> 
        /// <param name="context">GeometryValidationContext because of faults</param>  
        public void selfIntersect(Geometry geometry, int geometriesCounter, GeometryValidationContext context)
        {

            if (!geometry.STIsSimple())
            {
                // Meni se Idecka, pokud se GEOM nepovede ulozit, takze zatim schovavam
                // context.Faults.Add(new Intergraph.Emea.SmartClient.Data.GeometryValidationFault(Guid.NewGuid().ToString(), "Linie " + geometryToValidate.Attributes["ID"] + " kříží sama sebe", geometryToValidate.Geometry));
                context.Faults.Add(new Intergraph.Emea.SmartClient.Data.GeometryValidationFault(Guid.NewGuid().ToString(), "L" + geometriesCounter.ToString() + " kříží sama sebe", geometry));
            }

        }

        /// <summary>
        /// Validate geometry if it is inside area 
        /// </summary>
        /// <param name="geometry">Intergraph.Emea.SmartClient.Data.Geometry object</param>
        /// <param name="geometriesCounter">Count of geometries in context.Geometries, easier identification of fault objects</param> 
        /// <param name="context">GeometryValidationContext because of faults</param>  
        public void insideArea(GeometryMetadata geometryMetadata, int geometriesCounter, GeometryValidationContext context, IDatabaseProvider dbProvider, DbConnection dbConnection)
        {

            using (DbCommand cmd = dbProvider.CreateCommand("Select dil.ZAMEK_GEOMETRY as ZAMEKGEOMETRY From ELTM_DOK_DIL dil where dil.ID = @ID_DOK_DIL", dbConnection))
            {
                cmd.Parameters.Add(dbProvider.CreateParameter("ID_DOK_DIL", geometryMetadata.Attributes["ID_DOK_DIL"]));
                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    geometryUtils geomUtils = new geometryUtils();

                    while (reader.Read())
                    {
                        var zamekGeom = dbProvider.GetValue<Geometry>(reader, "ZAMEKGEOMETRY");
                        zamekGeom = geomUtils.roundGeometryCoords(zamekGeom, 3);

                        if (!geometryMetadata.Geometry.STWithin(zamekGeom.STBuffer(0.001)))
                        {
                            if (geometryMetadata.Geometry.GeometryType == GeometryType.Point) // pro body
                            {
                                context.Faults.Add(new Intergraph.Emea.SmartClient.Data.GeometryValidationFault(Guid.NewGuid().ToString(), "B" + geometriesCounter.ToString() + " není nakreslený uvnitř plochy zámku dané dok.", geometryMetadata.Geometry));

                            }
                            else // linie
                            {
                                context.Faults.Add(new Intergraph.Emea.SmartClient.Data.GeometryValidationFault(Guid.NewGuid().ToString(), "L" + geometriesCounter.ToString() + " není nakreslená uvnitř plochy zámku dané dok.", geometryMetadata.Geometry));
                            }

                        }

                    }
                }

            }

        }

        /// <summary>
        /// Fix the issue in GMSC, if the part of splitted element is deleted and next has no ID, it fails, this sets the new ID for this next element
        /// </summary>
        /// <param name="geometryMetadata">Intergraph.Emea.SmartClient.Data.GeometryMetadata object, 
        /// that contains geometry</param>
        /// <param name="sequenceName">Sequence name where to take the new ID</param> 
        /// <param name="idArrayList">ArrayList where the IDs from new geometries are stored, because of self interseting lines and persisted and non-persisted geometries</param>  
        public void fixSplitLine(GeometryMetadata geometryMetadata, string sequenceName, IDatabaseProvider dbProvider, DbConnection dbConnection, ArrayList idArrayList)
        {

            idArrayList.Add(geometryMetadata.Attributes["ID"]);

            if (idArrayList.Contains(geometryMetadata.Attributes["ID"]) && geometryMetadata.Identifier == null)
            {
                using (DbCommand seqNextValCmd = dbProvider.CreateCommand("select " + sequenceName + ".nextval from dual", dbConnection))
                {
                    geometryMetadata.Attributes["ID"] = seqNextValCmd.ExecuteScalar().ToString();
                }
            }

        }

        /// <summary>
        /// Fix the issue in GMSC, where parts of splitted element have doubled the first points
        /// </summary>
        /// <param name="geometryMetadata">Intergraph.Emea.SmartClient.Data.GeometryMetadata object, 
        /// that contains geometry</param>
        /// <param name="sequenceName">Sequence name where to take the new ID</param> 
        /// <param name="idArrayList">ArrayList where the IDs from new geometries are stored, because of self interseting lines and persisted and non-persisted geometries</param>  
        public Geometry fixSplitLineGeometryDoubledPoints(Geometry inGeometry)
        {

            GeometryBuilder fixedSplitLineGeomBuilder = new GeometryBuilder(0, 3);

            if (inGeometry.STStartPoint().ToString().Equals(inGeometry.STPointN(2).ToString()))
            {

                fixedSplitLineGeomBuilder.BeginGeometry(GeometryType.Polyline);
                fixedSplitLineGeomBuilder.BeginFigure(inGeometry.STStartPoint().STX.Value, inGeometry.STStartPoint().STY.Value, Convert.ToDouble(inGeometry.STStartPoint().Z.Value));

                for (int k = 3; k <= inGeometry.STNumPoints(); k++)
                {
                    fixedSplitLineGeomBuilder.AddLine(inGeometry.STPointN(k).STX.Value, inGeometry.STPointN(k).STY.Value, Convert.ToDouble(inGeometry.STPointN(k).Z.Value));

                }
                fixedSplitLineGeomBuilder.EndFigure();
                fixedSplitLineGeomBuilder.EndGeometry();

                return fixedSplitLineGeomBuilder.ConstructedGeometry;
            }
            else
            {
                return inGeometry;
            }


        }

        /// <summary>
        /// Control if all mandatory attributes are ready  
        /// </summary>
        /// <param name="geometryMetadata">Intergraph.Emea.SmartClient.Data.GeometryMetadata object, 
        /// that contains geometry</param>
        /// <param name="context">GeometryValidationContext because of faults</param>  
        public void geomAttributesOk(GeometryMetadata geometryMetadata, GeometryValidationContext context)
        {

            if (geometryMetadata == null)
                context.Faults.Add(new Intergraph.Emea.SmartClient.Data.GeometryValidationFault(Guid.NewGuid().ToString(), "Neexistuje žádná geometrie pro validaci"));
            if (!geometryMetadata.Attributes.ContainsKey("ID_DOK_DIL") || geometryMetadata.Attributes["ID_DOK_DIL"] == null)
                context.Faults.Add(new Intergraph.Emea.SmartClient.Data.GeometryValidationFault(Guid.NewGuid().ToString(), "Při vytváření geometrie nebyl aktivovaný žádný díl. Takto vytvořenou geometrii nelze uložit"));


        }

        /// <summary>
        /// Control if the line go only over the defined points
        /// </summary>
        /// <param name="geometry">Intergraph.Emea.SmartClient.Data.Geometry object</param>
        /// <param name="geometriesCounter">Count of geometries in context.Geometries</param> 
        /// <param name="context">GeometryValidationContext because of faults</param>  
        public void zLevelPointEquality(GeometryMetadata geometryMetadata, int geometriesCounter, GeometryValidationContext context, IDatabaseProvider dbProvider, DbConnection dbConnection)
        {

            geometryUtils geomUtils = new geometryUtils();

            logger.DebugFormat("Check Z level equality for line: {0}, VYSKOPIS: {1}", geometryMetadata.Geometry.AsTextZM(), geometryMetadata.Attributes["VYSKOPIS"].ToString());

            for (int i = 1; i <= geometryMetadata.Geometry.STNumPoints(); i++)
            {

                logger.DebugFormat("Check vertex {0}, X: {1}, Y: {2}, Z: {3} on line", i, geometryMetadata.Geometry.STPointN(i).STX, geometryMetadata.Geometry.STPointN(i).STY, geometryMetadata.Geometry.STPointN(i).Z);

                using (DbCommand cmd = dbProvider.CreateCommand(@"(select ID, GEOMETRY,VYSKOPIS, ID||'TMPODROBNYBOD_B' as UNIQUEID  from INGR_ZSGD.TMPODROBNYBOD_B podrb    
                                                                            where SDO_ANYINTERACT(podrb.GEOMETRY,SDO_UTIL.FROM_WKTGEOMETRY(@GEOMETRYTOVALIDATE)) = 'TRUE') 
                                                                                UNION ALL                               
                                                                            (select ID, GEOMETRY, to_number(nvl(replace(replace(dokb.VYSKOPIS,'Z_IS_NULL',0),'Z',''),0)) as VYSKOPIS ,ID||'ELTM_DOK_GRAFIKA_BOD' as UNIQUEID from ELTM_DOK_GRAFIKA_BOD dokb 
                                                                            where SDO_ANYINTERACT(dokb.GEOMETRY,SDO_UTIL.FROM_WKTGEOMETRY(@GEOMETRYTOVALIDATE)) = 'TRUE') ", dbConnection))
                {
                    //je potreba vyhodit Zkove souradnice, pokud jsou, protoze oracle nema ve WKT podporu 3D
                    RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace;
                    cmd.Parameters.Add(dbProvider.CreateParameter("GEOMETRYTOVALIDATE", Regex.Replace(geometryMetadata.Geometry.STPointN(i).AsTextZM().Replace(" 0", ""), "[\\s]{1}[0-9]{1,4}[\\.]{0,1}[0-9]*", "", options)));
                    //cmd.Parameters.Add(dbProvider.CreateParameter("ID", geometryToValidate.Attributes["ID"]));
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            int okCount = 0;
                            int zOkCount = 0;

                            while (reader.Read())
                            {
                                var pointGeomPodrbAndDokb = dbProvider.GetValue<Geometry>(reader, "GEOMETRY");
                                var vyskopisPodrbAndDokb = dbProvider.GetValue<string>(reader, "VYSKOPIS");

                                pointGeomPodrbAndDokb = geomUtils.roundGeometryCoords(pointGeomPodrbAndDokb, 3);

                                logger.DebugFormat("Check vertex {0} against podrbPoint geometry: {1}, vyskopis: {2}", i, pointGeomPodrbAndDokb.AsTextZM(), vyskopisPodrbAndDokb);


                                if (geometryMetadata.Geometry.STPointN(i).STX == pointGeomPodrbAndDokb.STX && geometryMetadata.Geometry.STPointN(i).STY == pointGeomPodrbAndDokb.STY && geometryMetadata.Geometry.STPointN(i).Z == pointGeomPodrbAndDokb.Z)
                                {
                                    // pozor, kdyby se zacaly rozsirovat kody na vice nez 2 oddelene carkou, musi se to tady zmenit    
                                    if ((vyskopisPodrbAndDokb.ToString() == geometryMetadata.Attributes["VYSKOPIS"].ToString().Split(',').First()) || (vyskopisPodrbAndDokb.ToString() == geometryMetadata.Attributes["VYSKOPIS"].ToString().Split(',').Last()))
                                    {
                                        okCount = okCount + 1;
                                    }

                                    if (pointGeomPodrbAndDokb.Z > 0)
                                    {
                                        zOkCount = zOkCount + 1;
                                    }
                                    //Tahle podmínka platí, pokud je v daném místě ještě bod s jinou Zovou výškou - PUVODZ
                                    //else
                                    //{
                                    //    context.Faults.Add(new Intergraph.Emea.SmartClient.Data.GeometryValidationFault(Guid.NewGuid().ToString(), "V lom. bodě " + i + " neprochází linie def. výškopisem"+geometryToValidate.Attributes["VYSKOPIS"].ToString(), geometryToValidate.Geometry));
                                    //}

                                }
                                // Tohle pravděpodobně nikdy nenastane
                                else
                                {

                                }
                            }

                            if (okCount < 1)
                            {
                                context.Faults.Add(new Intergraph.Emea.SmartClient.Data.GeometryValidationFault(Guid.NewGuid().ToString(), "L" + geometriesCounter.ToString() + " v lom. bodě " + i + " neprochází def. výškopisem " + geometryMetadata.Attributes["VYSKOPIS"].ToString(), geometryMetadata.Geometry));
                            }

                            if (zOkCount < 1)
                            {
                                context.Faults.Add(new Intergraph.Emea.SmartClient.Data.GeometryValidationFault(Guid.NewGuid().ToString(), "L" + geometriesCounter.ToString() + " v lom. bodě " + i + " prochází bodem s výškou 0 ", geometryMetadata.Geometry));
                            }
                        }
                        else
                        {
                            context.Faults.Add(new Intergraph.Emea.SmartClient.Data.GeometryValidationFault(Guid.NewGuid().ToString(), "L" + geometriesCounter.ToString() + " v lom. bodě " + i + " neprochází def. výškopisem " + geometryMetadata.Attributes["VYSKOPIS"].ToString(), geometryMetadata.Geometry));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Add Z coordinate from DMT to point geometry
        /// </summary>
        /// <param name="geometryMetadata">Intergraph.Emea.SmartClient.Data.Geometry object, 
        /// that contains point geometry</param>
        /// <return> geometry with new Z coordinate</return>
        public Geometry setZCoordinateFromDMT(Geometry geometry)
        {

            if (geometry.STGeometryType() == "Point")
            {

                // get X, Y coordinate of point
                double coordX = Convert.ToDouble(geometry.STX.Value.ToString());
                double coordY = Convert.ToDouble(geometry.STY.Value.ToString());
                double coordZ = 0;

                // create request for Z coordinate from DMT

                string beg = "http://mpp.praha.eu/arcgis3/rest/services/APP_GeoProcessing/XY_GetZ/GPServer/XY_GetZ/execute?";
                string location = "location_point=" + coordX.ToString() + "+" + coordY.ToString() + "+&";
                string end = "env%3AoutSR=&env%3AprocessSR=&returnZ=false&returnM=false&f=pjson";
                string url = beg + location + end;
                Uri uri = new Uri(url);
                coordZ = getResponse(uri);

                // add Z coordinate to geometry


                GeometryBuilder testGeomBuilder = new GeometryBuilder(0, geometry.Dimension);
                testGeomBuilder.BeginGeometry(geometry.GeometryType);

                if (geometry.Dimension > 2)
                {

                    testGeomBuilder.BeginFigure(coordX, coordY, coordZ);
                    testGeomBuilder.EndFigure();

                    testGeomBuilder.EndGeometry();

                    return testGeomBuilder.ConstructedGeometry;

                }
                else
                {
                    return geometry;
                }


            }

            return geometry;

        }

        private double getResponse(Uri url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);
            //DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(ZData));
            var res = JsonConvert.DeserializeObject<dynamic>(content);
            string z = res.results[0].value; // TODO: kontroly
            z.Replace(',', '.');
            double cz = Convert.ToDouble(z);

            return cz;

        }


    }

}
